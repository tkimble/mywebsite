import { Component, h } from "../preact/src/preact";
export class ImageThumb extends Component {
    constructor(props, context) { super(props, context); }
    render(props) {
        let clickListener = function (evt) {
            props.parentGallery.requestToggle(props.indexInGallery); //note to self: this.props doesn't work, have to rely on props passed to render
        };
        return (h("a", { onClick: clickListener },
            h("img", { src: props.thumbUrl, class: "gal-thumb-img" })));
    }
}
export class ImageExpand extends Component {
    constructor(props, context) { super(props, context); }
    render(props) {
        let separator1 = props.title ? h("br", null) : null;
        let titleObj = props.title ? h("div", { class: "gal-expand-title" }, props.title) : null;
        let separator2 = (props.title && props.caption) ? h("br", null) : null; //put a line break only if both title and caption are present
        let capObj = props.caption ? h("div", { class: "gal-expand-caption" }, props.caption) : null;
        return (h("div", { class: "gal-expand" },
            h("div", { class: "gal-expand-img-container" },
                h("img", { src: props.imageUrl, class: "gal-expand-img" })),
            separator1,
            titleObj,
            separator2,
            capObj));
    }
}
export class ImageGallery extends Component {
    constructor(props, context) {
        super(props, context);
        let imageObjects = [];
        for (let c = 0; c < props.initImages.length; c++) {
            let image = props.initImages[c];
            imageObjects[c] = {
                thumb: h(ImageThumb, {
                    thumbUrl: image.thumbUrl,
                    parentGallery: this,
                    indexInGallery: c
                }),
                expand: h(ImageExpand, {
                    title: image.title,
                    caption: image.caption,
                    imageUrl: image.imageUrl
                })
            };
        }
        this.setState({
            expandedIndex: -1,
            imageObjects: imageObjects
        });
    }
    render(props, state) {
        let imageThumbs = [];
        for (let image of state.imageObjects)
            imageThumbs.push(image.thumb);
        let sidebar = state.expandedIndex >= 0 ? state.imageObjects[state.expandedIndex].expand : null; //insert relevant expand if any
        return (h("div", { class: "gal" },
            h("div", { class: "gal-thumb-container" }, imageThumbs),
            sidebar));
    }
    requestToggle(index) {
        if (this.state.expandedIndex != index) //clicked on another image, so expand it
            this.setState({ expandedIndex: index });
        else //clicked on the expanded image, so collapse it
            this.setState({ expandedIndex: -1 });
        console.log("toggled index " + index);
    }
}
export function parseImages(images, cdnBase) {
    let list = [];
    for (let imf of images) {
        list.push({
            imageUrl: cdnBase + imf.url,
            thumbUrl: cdnBase + (imf.thumb ? imf.thumb : imf.url + ".thumb128.png"),
            title: (imf.title ? imf.title : null),
            caption: (imf.caption ? imf.caption : null)
        });
    }
    return list;
}
