import {Component, h, VNode, ComponentChildren} from "../preact/src/preact";

//TODO allow for multiple images in one item
export type ITProps = {
	thumbUrl : string,
	parentGallery : ImageGallery,
	indexInGallery : number
};
export class ImageThumb extends Component<ITProps>{
	constructor(props : ITProps, context? : any){super(props, context);}
	render(props : ITProps) {
		let clickListener = function(evt){
			props.parentGallery.requestToggle(props.indexInGallery); //note to self: this.props doesn't work, have to rely on props passed to render
		}
		return (
			<a onClick={clickListener}>
				<img src={props.thumbUrl} class="gal-thumb-img" />
			</a>);
	}
}

export type IEProps = {
	title : ComponentChildren,
	caption : ComponentChildren,
	imageUrl : string,
};
export class ImageExpand extends Component<IEProps>{
	constructor(props : IEProps, context? : any){super(props, context);}
	render(props : IEProps){
		let separator1 = props.title ? <br /> : null;
		let titleObj = props.title ? <div class="gal-expand-title">{props.title}</div> : null;
		let separator2 = (props.title && props.caption) ? <br /> : null; //put a line break only if both title and caption are present
		let capObj = props.caption ? <div class="gal-expand-caption">{props.caption}</div> : null;
		return(
			<div class="gal-expand">
				<div class="gal-expand-img-container">
					<img src={props.imageUrl} class="gal-expand-img" />
				</div>
				{separator1}
				{titleObj}
				{separator2}
				{capObj}
			</div>
		);
	}
}


export type IGProps = {
	initIndex : number,
	initImages : ImageProp[]
};
export type IGState = {
	/** The index of the expanded image, or a negative number if none is expanded */
	expandedIndex : number,
	imageObjects : {thumb : VNode, expand : VNode}[],
}
export type ImageProp = {
	imageUrl : string,
	thumbUrl : string,
	title :  ComponentChildren,
	caption :  ComponentChildren
}
export class ImageGallery extends Component<IGProps, IGState>{
	constructor(props : IGProps, context? : any){
		super(props, context);

		let imageObjects : {thumb : VNode, expand : VNode}[] = [];
		for(let c = 0; c < props.initImages.length; c++)
		{
			let image = props.initImages[c];
			imageObjects[c] = {
				thumb : h(
					ImageThumb, 
					{
						thumbUrl : image.thumbUrl,
						parentGallery : this,
						indexInGallery : c
					}
				),
				expand : h(
					ImageExpand, 
					{
						title : image.title,
						caption : image.caption,
						imageUrl : 	image.imageUrl
					}
				)
			}
		}
		this.setState({
			expandedIndex : -1,
			imageObjects : imageObjects
		})
	}
	render(props : IGProps, state : IGState){
		let imageThumbs : VNode[] = [];
		for(let image of state.imageObjects) imageThumbs.push(image.thumb);

		let sidebar = state.expandedIndex >= 0 ? state.imageObjects[state.expandedIndex].expand : null; //insert relevant expand if any

		return (
			<div class="gal">
				<div class="gal-thumb-container">{imageThumbs}</div>
				{sidebar}
			</div>
		);
	}
	requestToggle(index : number){
		if(this.state.expandedIndex != index) //clicked on another image, so expand it
			this.setState({expandedIndex : index})
		else //clicked on the expanded image, so collapse it
			this.setState({expandedIndex : -1})
		console.log("toggled index " + index);
	}
}

export function parseImages(
	images : {
		url : string,
		thumb? : string,
		title? : ComponentChildren,
		caption? : ComponentChildren
	}[],
	cdnBase : string
) : ImageProp[]{
	let list : ImageProp[] = [];
	for(let imf of images){
		list.push({
			imageUrl : cdnBase + imf.url,
			thumbUrl : cdnBase + (imf.thumb ? imf.thumb : imf.url + ".thumb128.png"),
			title : (imf.title ? imf.title : null),
			caption : (imf.caption ? imf.caption : null)
		});
	}
	return list;
}