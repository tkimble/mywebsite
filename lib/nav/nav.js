export class NavNode {
    constructor(genesis, children, exclusiveExpand, urlHook) {
        this.genesis = document.createElement("span");
        this.genesis.className = "nav-node";
        this.parent = null;
        for (let node of genesis) {
            this.genesis.appendChild(node);
        }
        this.children = children;
        this.exclusiveExpand = exclusiveExpand;
        for (let child of this.children) {
            child.setParent(this);
        }
        if (urlHook) {
            UrlHook.register(urlHook, this);
            this.urlHook = urlHook;
        }
        else
            this.urlHook = undefined;
    }
    setParent(parent) {
        this.parent = parent;
    }
    signalChildExpansionAndContractChildrenExcept(child) {
        for (let target of this.children)
            if (target != child && target.exclusiveExpand)
                target.contract();
    }
    getGenesis() { return this.genesis; }
    expand(navOnly) {
        if (this.urlHook)
            UrlHook.signalExpand(this.urlHook);
    }
    ;
    highlightGenesis(highlighted) {
        if (this.exclusiveExpand) {
            if (highlighted) {
                this.genesis.classList.add("nav-highlighted");
            }
            else {
                this.genesis.classList.remove("nav-highlighted");
            }
        }
    }
}
export class PanelNode extends NavNode {
    constructor(navPanel, sidePanel, genesis, children, exclusiveExpand, urlHook) {
        super(genesis, children, exclusiveExpand, urlHook);
        this.navPanel = navPanel;
        this.sidePanel = sidePanel;
        this.expanded = true;
    }
    expand(navOnly) {
        super.expand(navOnly);
        if (this.sidePanel && !navOnly)
            this.sidePanel.requestExpand();
        if (!this.expanded) {
            console.log("Expand!");
            if (this.navPanel)
                this.navPanel.expand();
            this.highlightGenesis(true);
            if (this.exclusiveExpand && this.parent)
                this.parent.signalChildExpansionAndContractChildrenExcept(this);
            this.expanded = true;
        }
    }
    contract() {
        //don't contract side panel, leave that for the env
        if (this.expanded) {
            console.log("Contract!");
            if (this.navPanel)
                this.navPanel.contract();
            this.highlightGenesis(false);
            this.expanded = false;
        }
    }
    navPlaceFlat(level, env) {
        if (this.navPanel) {
            if (level == 0) {
                env.appendChild(this.navPanel.getElement());
                return true;
            }
            else if (level > 0) {
                let anySucceeded = false;
                for (let child of this.children) {
                    anySucceeded = anySucceeded || child.navPlaceFlat(level - 1, env);
                }
                return anySucceeded;
            }
            else //shouldn't happen
                return false;
        }
        else
            return false;
    }
}
export class ClickablePanelNode extends PanelNode {
    constructor(navPanel, sidePanel, genesis, children, exclusiveExpand, urlHook) {
        let meh = document.createElement("a");
        super(navPanel, sidePanel, [meh], children, exclusiveExpand, urlHook);
        meh.addEventListener("click", this);
        for (let node of genesis)
            meh.appendChild(node);
    }
    handleEvent(evt) {
        console.log(evt.type);
        //contracts all children so that children pointing to other pages aren't highlighted.
        //TODO instead of this, overhaul panels so that a genesis' highlighted state is reactively bound to its corresponding panels (side and nav)'s expanded state
        for (let child of this.children)
            child.contract();
        if (this.exclusiveExpand) {
            if (this.expanded)
                this.contract();
            else
                this.expand();
        }
        else
            this.expand();
    }
}
export class NestedMenuNode extends PanelNode {
    constructor(sidePanel, genesis, children, exclusiveExpand, urlHook) {
        //this sandwiches a menunode, indented right, right after our genesis in its container.
        //multi-layered nested trees all store their children in a single-indented node, since the single indent is relative to its parent
        let chillen = new MenuContainerPanel(children, true);
        genesis.push(chillen.getElement());
        super(null, sidePanel, genesis, children, exclusiveExpand, urlHook);
        this.chillen = chillen;
    }
    expand() {
        if (!this.expanded) {
            this.chillen.expand();
        }
        super.expand();
    }
    contract() {
        if (this.expanded) {
            this.chillen.contract();
        }
        super.contract();
    }
}
export class ClickableNestedNode extends ClickablePanelNode {
    constructor(sidePanel, genesis, children, exclusiveExpand, urlHook) {
        let chillen = new MenuContainerPanel(children, true);
        genesis.push(chillen.getElement());
        super(null, sidePanel, genesis, children, exclusiveExpand, urlHook);
        this.chillen = chillen;
    }
    expand() {
        if (!this.expanded) {
            this.chillen.expand();
        }
        super.expand();
    }
    contract() {
        if (this.expanded) {
            this.chillen.contract();
        }
        super.contract();
    }
}
export class PanelEnvToken {
    constructor(env, panel) { this.env = env; this.panel = panel; }
    requestExpand() { this.env.requestExpand(this.panel); }
}
export class Environment {
    constructor(env) {
        this.env = env;
        this.panels = [];
    }
    addPanel(panel) {
        this.panels.push(panel);
        this.env.appendChild(panel.getElement());
        return new PanelEnvToken(this, panel);
    }
    requestExpand(panel) {
        for (let pnl of this.panels) {
            if (pnl == panel)
                pnl.expand();
            else
                pnl.contract();
        }
    }
}
export class BasicPanel {
    constructor(content) {
        this.content = content;
    }
    expand() {
        if (this.content)
            this.content.classList.remove("nav-hidden");
    }
    contract() {
        if (this.content)
            this.content.classList.add("nav-hidden");
    }
    getElement() { return this.content; }
}
export class MenuContainerPanel extends BasicPanel {
    constructor(children, indent) {
        if (indent)
            console.log("indent!");
        super(document.createElement("div"));
        this.content.className = indent ? "nav-childcontainer nav-indent" : "nav-childcontainer"; //assuming indent will evaluate as false if not provided
        for (let child of children) {
            this.content.appendChild(document.createElement("br"));
            this.content.appendChild(child.getGenesis());
        }
    }
}
export class IframePanel {
    constructor(url) {
        this.content = document.createElement("iframe");
        this.content.className = "nav-iframe";
        this.setUp = false;
        this.url = url;
    }
    expand() {
        this.ensureSetUp();
        this.content.classList.remove("nav-hidden");
    }
    contract() {
        this.content.classList.add("nav-hidden");
    }
    getElement() {
        return this.content;
    }
    ensureSetUp() {
        if (!this.setUp) { //happens ony once
            this.content.src = this.url;
            this.setUp = true;
        }
    }
}
export class NavPanel {
    getElement() { return this.obj; }
    constructor(root) {
        this.root = root;
        this.obj = document.createElement("div");
        this.obj.className = "nav";
        for (let c = 0; this.root.navPlaceFlat(c, this.obj); c++) { }
    }
    expand(navOnly) { this.root.expand(navOnly); }
    contract() { this.root.contract(); }
}
function undef(parameter, defaultVar) {
    if (typeof parameter === "undefined")
        return defaultVar;
    else
        return parameter;
}
export class NB {
    static menu(text, children, exclusiveExpand) {
        return this.menuX(text, null, children, undef(exclusiveExpand, true));
    }
    static menuX(text, sidePanel, children, exclusiveExpand, urlHook) {
        let foo = new ClickablePanelNode(new MenuContainerPanel(children), sidePanel, [formatMenu(text)], children, undef(exclusiveExpand, true), urlHook);
        foo.contract();
        return foo;
    }
    static menuNX(text, sidePanel, children, exclusiveExpand, urlHook) {
        let foo = new NestedMenuNode(sidePanel, [formatMenu(text)], children, undef(exclusiveExpand, true), urlHook);
        foo.contract();
        return foo;
    }
    static menuN(text, children, exclusiveExpand, urlHook) {
        return this.menuNX(text, null, children, exclusiveExpand, urlHook);
    }
    static endX(text, sidePanel, startExpanded, exclusiveExpand) {
        let foo = new ClickablePanelNode(null, sidePanel, [formatMenu(text)], [], undef(exclusiveExpand, true));
        foo.contract();
        return foo;
    }
    static link(text, url) {
        let obj = document.createElement("a");
        obj.setAttribute("href", url);
        obj.appendChild(formatMenu(text));
        let foo = new PanelNode(null, null, [obj], [], false);
        foo.contract();
        return foo;
    }
    static label(text) {
        let foo = new PanelNode(null, null, [formatMenu(text)], [], false);
        foo.contract();
        return foo;
    }
}
export function formatMenu(text) {
    return document.createTextNode(text + "\u00a0›");
}
export class UrlHook {
    static register(hook, node) {
        UrlHook.nodes.push({
            node: node,
            title: hook.title,
            hash: hook.hash
        });
        console.log("add" + hook.hash);
    }
    static expand(root) {
        let path = window.location.hash.substring(1); //gets the hash without the #
        console.log(window.location.hash + path);
        let target = undefined; //search for the relevant node
        for (let node of UrlHook.nodes) {
            console.log("look at" + node.hash);
            if (node.hash == path) {
                target = node.node;
                break;
            }
        }
        if (target) {
            UrlHook.expandUp(target);
            target.expand();
        }
        else {
            root.expand();
        }
    }
    static expandUp(hash) {
        hash.expand(true);
        if (hash.parent)
            UrlHook.expandUp(hash.parent);
    }
    static signalExpand(hook) {
        history.pushState({}, hook.title, "#" + hook.hash);
    }
}
UrlHook.nodes = [];
