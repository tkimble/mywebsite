//TODO: translate this whole mess into react or something
export interface Panel{
	expand() : void;
	contract() : void;
	getElement() : HTMLElement;
}

export abstract class NavNode{
	protected children : NavNode[];
	protected genesis : HTMLElement;
	public parent : NavNode;
	protected urlHook : {title : string, hash : string};
	constructor(genesis : Node[], children : NavNode[], exclusiveExpand : boolean, urlHook? : {title : string, hash : string}){
		this.genesis = document.createElement("span");
		this.genesis.className = "nav-node";
		this.parent = null;
		for(let node of genesis){
			this.genesis.appendChild(node); 
		}
		this.children = children;
		this.exclusiveExpand = exclusiveExpand;
		for(let child of this.children){
			child.setParent(this);
		}

		if(urlHook){
			UrlHook.register(urlHook, this);
			this.urlHook = urlHook;
		}else this.urlHook = undefined;
	}
	setParent(parent : NavNode){
		this.parent = parent;
	}
	signalChildExpansionAndContractChildrenExcept(child : NavNode){
		for(let target of this.children)
			if(target != child && target.exclusiveExpand)
				target.contract();
	}

	public getGenesis(): Element { return this.genesis; }
	
	public expand(navOnly? : boolean) : void{
		if(this.urlHook)
			UrlHook.signalExpand(this.urlHook);
	};
	public abstract contract() : void;

	highlightGenesis(highlighted : boolean) : void{
		if(this.exclusiveExpand){
			if(highlighted){
				this.genesis.classList.add("nav-highlighted");
			}else{
				this.genesis.classList.remove("nav-highlighted");
			}
		}
	}

	abstract navPlaceFlat(level : number, env : Element) : boolean;
	//public abstract isEnd : boolean; //this wouldn't be needed if we had ADTs
	public abstract expanded : boolean;
	public exclusiveExpand : boolean;
}
export class PanelNode extends NavNode {
	navPanel : Panel;
	sidePanel : PanelEnvToken;
	public expanded: boolean;
	constructor(navPanel : Panel, sidePanel : PanelEnvToken, genesis : Node[], children : NavNode[], exclusiveExpand : boolean, urlHook? : {title : string, hash : string}){
		super(genesis, children, exclusiveExpand, urlHook);
		this.navPanel = navPanel;
		this.sidePanel = sidePanel;
		this.expanded = true;
	}
	public expand(navOnly? : boolean): void {
		super.expand(navOnly);
		if(this.sidePanel && !navOnly) this.sidePanel.requestExpand();
		if(!this.expanded){
			console.log("Expand!")
			if(this.navPanel) this.navPanel.expand();
			this.highlightGenesis(true);
			if(this.exclusiveExpand && this.parent)
				this.parent.signalChildExpansionAndContractChildrenExcept(this);
			this.expanded = true;
		}
	}
	public contract(): void {
		//don't contract side panel, leave that for the env
		if(this.expanded){
			console.log("Contract!")
			if(this.navPanel) this.navPanel.contract();
			this.highlightGenesis(false);
			this.expanded = false;
		}
	}
	navPlaceFlat(level: number, env: Element): boolean {
		if(this.navPanel){
			if(level == 0){
				env.appendChild(this.navPanel.getElement());
				return true;
			}else if(level > 0){
				let anySucceeded = false;
				for(let child of this.children){
					anySucceeded = anySucceeded || child.navPlaceFlat(level - 1, env);
				}
				return anySucceeded;
			}else //shouldn't happen
				return false;
		}else
			return false;
	}
}
export class ClickablePanelNode extends PanelNode implements EventListenerObject{
	constructor(navPanel : Panel, sidePanel : PanelEnvToken, genesis : Node[], children : NavNode[], exclusiveExpand : boolean, urlHook? : {title : string, hash : string}){
		let meh = document.createElement("a")

		super(navPanel, sidePanel, [meh], children, exclusiveExpand, urlHook);

		meh.addEventListener("click", this);
		for(let node of genesis)
		meh.appendChild(node);
	}
	
	handleEvent(evt: Event): void {
		console.log(evt.type);
		//contracts all children so that children pointing to other pages aren't highlighted.
		//TODO instead of this, overhaul panels so that a genesis' highlighted state is reactively bound to its corresponding panels (side and nav)'s expanded state
		for(let child of this.children) child.contract();
		if(this.exclusiveExpand){
			if(this.expanded)
				this.contract();
			else
				this.expand();
		}else
			this.expand();
	}
}
export class NestedMenuNode extends PanelNode{
	chillen : MenuContainerPanel;
	constructor(sidePanel : PanelEnvToken, genesis : Node[], children : NavNode[], exclusiveExpand : boolean, urlHook? : {title : string, hash : string}){
		//this sandwiches a menunode, indented right, right after our genesis in its container.
		//multi-layered nested trees all store their children in a single-indented node, since the single indent is relative to its parent
		let chillen = new MenuContainerPanel(children, true);
		genesis.push(chillen.getElement());
		super(null, sidePanel, genesis, children, exclusiveExpand, urlHook);
		this.chillen = chillen; 
	}

	public expand(): void {
		if(!this.expanded){
			this.chillen.expand();
		}
		super.expand();
	}

	public contract(): void {
		if(this.expanded){
			this.chillen.contract();
		}
		super.contract();
	}
}
export class ClickableNestedNode extends ClickablePanelNode implements NestedMenuNode{
	chillen : MenuContainerPanel;
	constructor(sidePanel : PanelEnvToken, genesis : Node[], children : NavNode[], exclusiveExpand : boolean, urlHook? : {title : string, hash : string}){
		let chillen = new MenuContainerPanel(children, true);
		genesis.push(chillen.getElement());

		super(null, sidePanel, genesis, children, exclusiveExpand, urlHook);

		this.chillen = chillen; 
	}

	public expand(): void {
		if(!this.expanded){
			this.chillen.expand();
		}
		super.expand();
	}

	public contract(): void {
		if(this.expanded){
			this.chillen.contract();
		}
		super.contract();
	}
}
export class PanelEnvToken{
	env : Environment;
	panel : Panel;
	constructor(env : Environment, panel : Panel){this.env = env; this.panel = panel;}
	requestExpand() : void {this.env.requestExpand(this.panel);}
}
export class Environment{
	env : HTMLElement;
	constructor(env : HTMLElement){
		this.env = env;
		this.panels = [];
	}
	panels : Panel[];
	addPanel(panel : Panel) : PanelEnvToken{
		this.panels.push(panel);
		this.env.appendChild(panel.getElement());
		return new PanelEnvToken(this, panel);
	}
	requestExpand(panel : Panel){
		for(let pnl of this.panels){
			if(pnl == panel)
				pnl.expand();
			else
				pnl.contract();
		}
	}
}
export class BasicPanel implements Panel{
	protected content : HTMLElement;
	constructor(content : HTMLElement){ 
		this.content = content;
	}
	expand(): void {
		if(this.content)
			this.content.classList.remove("nav-hidden");
	}
	contract(): void {
		if(this.content)
			this.content.classList.add("nav-hidden");
	}
	getElement() {return this.content;}
}
export class MenuContainerPanel extends BasicPanel{ //TODO turn this into a function cause now it's nothing but a constructor
	constructor(children : NavNode[], indent? : boolean){
		if(indent)console.log("indent!");
		super(document.createElement("div"));
		this.content.className = indent ? "nav-childcontainer nav-indent" : "nav-childcontainer"; //assuming indent will evaluate as false if not provided
		for(let child of children){
			this.content.appendChild(document.createElement("br"));
			this.content.appendChild(child.getGenesis());
		}
	}
}
export class IframePanel implements Panel{
	protected content : HTMLIFrameElement;
	protected url : string;
	protected setUp : boolean;
	constructor(url : string){
		this.content = document.createElement("iframe");
		this.content.className = "nav-iframe";
		this.setUp = false;
		this.url = url;
	}
	expand(): void {
		this.ensureSetUp();
		this.content.classList.remove("nav-hidden");
	}
	contract(): void {
		this.content.classList.add("nav-hidden");
	}
	getElement(): HTMLElement {
		return this.content;
	}
	ensureSetUp(){
		if(!this.setUp){ //happens ony once
			this.content.src = this.url;
			this.setUp = true;
		}
	}
}
export class NavPanel implements Panel{
	protected root : NavNode;
	protected obj : HTMLElement; getElement(){return this.obj;}
	constructor(root: NavNode){
		this.root = root;
		this.obj = document.createElement("div");
		this.obj.className = "nav";
		
		for(let c = 0; this.root.navPlaceFlat(c, this.obj); c++){}
	}
	expand(navOnly? : boolean) {this.root.expand(navOnly);}
	contract(){this.root.contract();}
}
function undef<T>(parameter : T|undefined, defaultVar : T) : T{
    if (typeof parameter === "undefined")
        return defaultVar;
    else 
        return <T>parameter;
}
export class NB{
	static menu(text : string, children : NavNode[], exclusiveExpand? : boolean) : NavNode{
		return this.menuX(text, null, children, undef(exclusiveExpand, true));
	}
	static menuX(text : string, sidePanel : PanelEnvToken, children : NavNode[], exclusiveExpand? : boolean, urlHook? : {title : string, hash : string}){
		let foo = new ClickablePanelNode(
			new MenuContainerPanel(children),
			sidePanel,
			[formatMenu(text)],
			children,
			undef(exclusiveExpand, true),
			urlHook
		)
		foo.contract();
		return foo;
	}
	static menuNX(text : string, sidePanel : PanelEnvToken, children : NavNode[], exclusiveExpand? : boolean, urlHook? : {title : string, hash : string}) : NavNode{
		let foo = new NestedMenuNode(sidePanel, [formatMenu(text)], children, undef(exclusiveExpand, true), urlHook);
		foo.contract();
		return foo;
	}
	static menuN(text : string, children : NavNode[], exclusiveExpand? : boolean, urlHook? : {title : string, hash : string}) : NavNode{
		return this.menuNX(text, null, children, exclusiveExpand, urlHook);
	}
	static endX(text : string, sidePanel : PanelEnvToken, startExpanded? : boolean, exclusiveExpand? : boolean){
		let foo = new ClickablePanelNode(
			null,
			sidePanel,
			[formatMenu(text)],
			[],
			undef(exclusiveExpand, true)
		)
		foo.contract();
		return foo;
	}
	static link(text : string, url : string) : NavNode{
		let obj = document.createElement("a");
		obj.setAttribute("href", url);
		obj.appendChild(formatMenu(text));
		let foo = new PanelNode(
			null,
			null,
			[obj],
			[],
			false
		);
		foo.contract();
		return foo;
	}
	static label(text : string) : NavNode{
		let foo = new PanelNode(
			null,
			null,
			[formatMenu(text)],
			[],
			false
		);
		foo.contract();
		return foo;
	}
}   

export function formatMenu(text : String) : Node{
	return document.createTextNode(text + "\u00a0›");
}

export class UrlHook{
	static nodes : {node : NavNode, title : string, hash : string}[] = [];
	static register(hook : {title : string, hash : string}, node : NavNode) : void{
		UrlHook.nodes.push({
			node : node,
			title : hook.title,
			hash : hook.hash
		});
		console.log("add" + hook.hash);
	}
	static expand(root : NavNode) : void{
		let path = window.location.hash.substring(1); //gets the hash without the #
		console.log(window.location.hash + path);
		
		let target : NavNode = undefined; //search for the relevant node
		for(let node of UrlHook.nodes){
			console.log("look at" + node.hash);
			if(node.hash == path){
				target = node.node;
				break;
			}
		}
		if(target){
			UrlHook.expandUp(target);
			target.expand();
		}else{
			root.expand();
		}
	}
	static expandUp(hash : NavNode){
		hash.expand(true);
		if(hash.parent)
			UrlHook.expandUp(hash.parent);
	}
	static signalExpand(hook : {title : string, hash : string}){
		history.pushState({}, hook.title, "#" + hook.hash);
	}
}