import {ImageGallery, IGProps, ImageProp, parseImages} from "../../lib/gallery/gal"
import {render, h} from "../../lib/preact/src/preact"

const cdnBase = "/big_static/mywebsite/portfoliopics/design/"

let imageFiles = [
	{
		url : "Cage Lantern.jpg", 
		title : "Cage Lantern",
		caption : "ceramic. 2017"
	},
	{
		url : "Paleolith.jpg", 
		title : "Paleolith",
		caption : "poplar, plywood, fabric. 2017"
	},
	{
		url : "Hong Kong.jpg", 
		title : "Hong Kong",
		caption : "digital, vector. 2016"
	},
	{
		url : "Polylingua (cover).png", 
		title : "Polylingua",
		caption : [
			"print. 2018", <br />, 
			<a href="https://1drv.ms/b/s!AsRvI5m4O4NX7lLSDAIfBExvCfEZ">Read here</a>, <br />,
			"a magazine of foreign language pieces"
		]
	},
	{
		url : "High School/Blood Drive.png",
		title : "Blood Drive",
		caption : "print, greyscale. 2017"
	},
	{
		url : "High School/Blood Drive 2.png",
		title : "Blood Drive (2)",
		caption : "print. 2016"
	},
	{
		url : "High School/Blood Drive 3.png",
		title : "Blood Drive (3)",
		caption : "print, greyscale. 2016"
	},
	{
		url : "High School/Candycane.jpg",
		title : "Candy Canes",
		caption : [
			"print, 2017", <br />,
			"a fundraising item involving packaged candy canes with messages attached"
		]
	},
	{
		url : "High School/Club Fair.png",
		title : "Club Fair",
		caption : "print, greyscale. 2016"
	},
	{
		url : "High School/Club Fair 2.png",
		title : "Club Fair (2)",
		caption : "screen. 2017"
	},
	{
		url : "High School/Halloween.png",
		title : "Halloween",
		caption : "print. 2016"
	},
	{
		url : "High School/Halloween 2.png",
		title : "Halloween (2)",
		caption : "print. 2017"
	},
	{
		url : "High School/Schedule.png",
		title : "Schedule",
		caption : "print, greyscale. 2018"
	},
	{
		url : "High School/Spirit Week.png",
		title : "Spirit Week",
		caption : "print. 2017"
	},
	{
		url : "High School/Thanksgiving.png",
		title : "Thanksgiving",
		caption : "print. 2016"
	},
]

render(
	h<IGProps>(
		ImageGallery, 
		{
			initIndex : -1,
			initImages : parseImages(imageFiles, cdnBase)
		}
	), 
	document.getElementById("gallery-here")
);