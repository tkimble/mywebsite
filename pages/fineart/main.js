import { ImageGallery, parseImages } from "../../lib/gallery/gal";
import { render, h } from "../../lib/preact/src/preact";
const cdnBase = "/big_static/mywebsite/portfoliopics/fineart/";
let imageFiles = [
    {
        url: "Beech.jpg",
        title: "Beech",
        caption: "ink, acrylic. 2017"
    },
    {
        url: "Burnt Landscape.jpg",
        title: "Burnt Landscape",
        caption: [
            "cardboard relief. 2018", h("br", null),
            "based on the topography of Schuylkill County, PA, in the middle of anthracite coal country"
        ]
    },
    {
        url: "Empty Units.JPG",
        title: "Empty Units",
        caption: [
            "ink on paper. 2018", h("br", null),
            "inspired by what's going on vis-à-vis gentrification here in boston"
        ]
    },
    {
        url: "Fire Halls.JPG",
        title: "",
        caption: "oil. 2017"
    },
    {
        url: "Games.JPG",
        title: "Games",
        caption: [
            "gouache on paper. 2017", h("br", null),
            "based on a dutch golden age painting of various dead game"
        ]
    },
    {
        url: "Ghostly Memories.jpg",
        title: "Ghosts",
        caption: [
            "oil, sculpted canvas. 2018", h("br", null),
            "thinking back childhood homes"
        ]
    },
    {
        url: "Grass Mask.jpg",
        title: "Grass Mask",
        caption: "ceramic. 2016"
    },
    {
        url: "Landscaping.JPG",
        title: "Landscaping",
        caption: [
            "oil. 2018", h("br", null),
            "\"yeah, pine trees are indigenous to this single suburban tract in sunny Arizona.\""
        ]
    },
    {
        url: "Novye Cheryomushki.JPG",
        title: "Novye Cheryomushki",
        caption: [
            "oil. 2018", h("br", null),
            "a run-down but nostalgic housing estate in ",
            h("a", { href: "https://www.calvertjournal.com/features/show/4235/soviet-mass-housing-novye-cheryomushki-belyayevo-suburbs", target: "_parent" }, "Novye Cheryomushki, Moscow")
        ]
    },
    {
        url: "Office Park.JPG",
        title: "Office Park",
        caption: "ink, gouache on paper. 2017"
    },
    {
        url: "PB_J.jpg",
        title: "PB&J",
        caption: "gouache, acrylic on paper. 2017"
    },
    {
        url: "Pieta.jpg",
        title: "Pietà",
        caption: "charcoal on salted paper. 2017"
    },
    {
        url: "Rock Landscape.jpg",
        title: "",
        caption: "pastel. 2016"
    },
    {
        url: "Shandong Home.jpg",
        title: "Shandong",
        caption: [
            "oil. 2017", h("br", null),
            "my paternal grandparents' rural home in Shandong Province, China"
        ]
    },
    {
        url: "String Christ.JPG",
        title: "String Christ",
        caption: "charcoal. 2017"
    },
    {
        url: "Train Station.jpg",
        title: "Train Station",
        caption: [
            "charcoal, pencil collage. 2017", h("br", null),
            "waiting for a ride at the Rt. 128 commuter rail station"
        ]
    },
    {
        url: "Trashtic.JPG",
        title: "Fantastrash",
        caption: "mixed media. 2017"
    }
];
render(h(ImageGallery, {
    initIndex: -1,
    initImages: parseImages(imageFiles, cdnBase)
}), document.getElementById("gallery-here"));
