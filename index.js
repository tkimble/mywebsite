import { Environment, IframePanel, NB, NavPanel, UrlHook } from "./lib/nav/nav";
(function () {
    console.time("setup");
    let env = new Environment(document.getElementById("panels"));
    let mainpanel = env.addPanel(new IframePanel("/pages/intro/main.html"));
    let portfolio = env.addPanel(new IframePanel("/pages/portfolio/main.html"));
    let fineart = env.addPanel(new IframePanel("/pages/fineart/main.html"));
    let design = env.addPanel(new IframePanel("/pages/design/main.html"));
    let code = env.addPanel(new IframePanel("/pages/code/main.html"));
    let about = env.addPanel(new IframePanel("/pages/aboutme/main.html"));
    let tree = NB.menu("", [
        NB.menuX("Peter Yu's Own Personal Web-Site For Employers And Others Who May Consider Giving Him Money", mainpanel, [
            NB.menuX("Portfolio", portfolio, [
                NB.menuX("Fine Art", fineart, [], true, { title: "fine art", hash: "fineart" }),
                NB.menuX("Design", design, [], true, { title: "design", hash: "design" }),
                NB.menuX("Code", code, [], true, { title: "code", hash: "code" })
            ], true, { title: "portfolio", hash: "portfolio" }),
            NB.menuX("About me", about, [], true, { title: "about me", hash: "about" })
        ], true, { title: "peter yu", hash: "intro" })
    ]);
    document.getElementById("nav-here").appendChild(new NavPanel(tree).getElement());
    UrlHook.expand(tree);
    console.timeEnd("setup");
})();
